package adventofcode2015;

import java.util.Scanner;
//256 muy alto

public class Day5DoesnHeHaveInternElvesForThis {

    public void Day5DoesnHeHaveInternElvesForThis() {
        Scanner scan = new Scanner(System.in);
        String sText = "";
        int iContValidations = 0;
        while (!sText.equals("00")) {
            sText = scan.next();
            if (repeatDoubly(sText) && repeatDifferentOne(sText)) {
                iContValidations++;
            }
        }
        System.out.println(iContValidations);
    }

    public boolean vowels(String sValue) {
        int iContVowels = 0;
        for (int i = 0; i < sValue.length(); i++) {
            if (sValue.charAt(i) == 'a' || sValue.charAt(i) == 'e' || sValue.charAt(i) == 'i'
                    || sValue.charAt(i) == 'o' || sValue.charAt(i) == 'u') {
                iContVowels++;
            }
            if (iContVowels == 3) {
                return true;
            }
        }
        return false;
    }

    public boolean repeatLetters(String sValue) {
        char cLetter = 'a';
        for (int i = 0; i < 27; i++) {
            StringBuffer sLetter = new StringBuffer();
            sLetter.append(cLetter + "" + cLetter);
//            sLetter.append(cLetter);
            if (sValue.indexOf(sLetter.toString()) != -1) {
                return true;
            }
            cLetter++;
        }
        return false;
    }

    public boolean searchWordsProhibited(String sValue) {
        if (sValue.indexOf("ab") != -1 || sValue.indexOf("cd") != -1
                || sValue.indexOf("pq") != -1 || sValue.indexOf("xy") != -1) {
            return false;
        }
        return true;
    }

    public boolean repeatDoubly(String sValue) {
        for (int i = 0; i < 27; i++) {
            if (i + 1 < sValue.length()) {
                StringBuffer sLetters = new StringBuffer();
                sLetters.append(sValue.charAt(i) + "" + sValue.charAt(i + 1));
                if (sValue.lastIndexOf(sLetters.toString()) > i + 1) {
                    return true;
                }
            }
        }
        return false;
    }

    public boolean repeatDifferentOne(String sValue) {
        for (int i = 0; i < sValue.length(); i++) {
            if (i + 2 < sValue.length() && sValue.charAt(i) == sValue.charAt(i + 2)) {
                return true;
            }
        }
        return false;
    }
}
