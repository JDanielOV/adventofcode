package adventofcode2015;

import java.util.Arrays;
import java.util.Scanner;
import java.util.Vector;

public class Day2IWasToldThereWouldBeNoMath {

    public void Day2IWasToldThereWouldBeNoMath() {
        Scanner scan = new Scanner(System.in);
        Vector<String> vList = new Vector();
        String sDimensions = "";
        int iSumPaper = 0;
        while (!sDimensions.equals("00")) {
            sDimensions = scan.next();
            vList.add(sDimensions);
        }
        vList.remove(vList.size() - 1);
        for (String string : vList) {
            iSumPaper += ((Integer.parseInt(string.substring(0, string.indexOf("x"))))
                    * (Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x"))))
                    * (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))));
            iSumPaper += minimunSizeRibbon((Integer.parseInt(string.substring(0, string.indexOf("x")))),
                     (Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x")))),
                     (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))));
//            iSumPaper += (2 * (Integer.parseInt(string.substring(0, string.indexOf("x")))) * (Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x")))))
//                    + (2 * (Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x")))) * (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))))
//                    + (2 * (Integer.parseInt(string.substring(0, string.indexOf("x")))) * (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))));
//            iSumPaper += minimunSize((Integer.parseInt(string.substring(0, string.indexOf("x")))) * (Integer.parseInt(string.substring(string.indexOf("x") + 1, string.lastIndexOf("x")))), (Integer.parseInt(string.substring(string.indexOf("x") + 1,
//                    string.lastIndexOf("x")))) * (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))),
//                    (Integer.parseInt(string.substring(0, string.indexOf("x")))) * (Integer.parseInt(string.substring(string.lastIndexOf("x") + 1))));
        }
        System.out.println(iSumPaper);
    }

    public int minimunSize(int lw, int wh, int hl) {
        int iArr[] = {lw, wh, hl};
        Arrays.sort(iArr);
        return iArr[0];
    }

    public int minimunSizeRibbon(int l, int w, int h) {
        int iArr[] = {l, w, h};
        Arrays.sort(iArr);
        return (2 * (iArr[0])) + (2 * (iArr[1]));
    }
}
