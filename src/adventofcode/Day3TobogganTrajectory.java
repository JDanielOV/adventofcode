package adventofcode;

import java.util.Vector;
import java.util.Scanner;

public class Day3TobogganTrajectory {

    public void Day3TobogganTrajectory() {
        Scanner scan = new Scanner(System.in);
        Vector<String> vMap = new Vector();
        String sPedir;
        while (scan.hasNext()) {
            sPedir = scan.next();
            if (sPedir.equals("--")) {
                break;
            }
            vMap.add(sPedir);
            //..#..#.#   //9      //7
            //..#.....
        }
        int iTrackerMap = 0;
        int iContTree = 0;
        for (int i = 1; i < vMap.size(); i++) {
            if (iTrackerMap + 3 < vMap.get(i).length()) {
                iTrackerMap += 3;
                if (vMap.get(i).charAt(iTrackerMap) == '#') {
                    System.out.println(vMap.get(i));
                    iContTree++;
                }
            } else if (iTrackerMap + 3 > vMap.get(i).length() - 1) {
                iTrackerMap = (iTrackerMap + 3) - vMap.get(i).length();
                if (vMap.get(i).charAt(iTrackerMap) == '#') {
                    System.out.println(vMap.get(i));
                    iContTree++;
                }
            }
        }
        System.out.println(iContTree);
    }
}
