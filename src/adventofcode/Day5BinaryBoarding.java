package adventofcode;

import java.util.Scanner;

public class Day5BinaryBoarding {

    public void Day5BinaryBoarding() {
        Scanner scan = new Scanner(System.in);
        String sWord = scan.next();
        int iMaximun = 0;
        while (!sWord.equals("00")) {
            if ((ValueRows(sWord) + ValueColumn(sWord)) > iMaximun) {
                iMaximun = (ValueRows(sWord) + ValueColumn(sWord));
            }
            sWord=scan.next();
        }
        System.out.println(iMaximun);
    }

    public int ValueRows(String sValue) {
        double iTemporal = 0.00;
        double iArrRows[] = {0.00, 127.00};
        for (int i = 0; i < 7; i++) {
            if (sValue.charAt(i) == 'F') {
                iTemporal = Math.floor((iArrRows[1] - iArrRows[0]) / 2);
                iArrRows[1] = (iTemporal + iArrRows[0]);
            } else if (sValue.charAt(i) == 'B') {
                iTemporal = Math.ceil((iArrRows[1] - iArrRows[0]) / 2);
                iArrRows[0] = (iTemporal + iArrRows[0]);
            }
        }
        return (int) (iArrRows[0] * 8);
    }

    public int ValueColumn(String sValue) {
        double iTemporal = 0.00;
        double iArrColumns[] = {0, 7};
        for (int i = 7; i < sValue.length(); i++) {
            if (sValue.charAt(i) == 'L') {
                iTemporal = Math.floor((iArrColumns[1] - iArrColumns[0]) / 2);
                iArrColumns[1] = iTemporal + iArrColumns[0];
            } else if (sValue.charAt(i) == 'R') {
                iTemporal = Math.ceil((iArrColumns[1] - iArrColumns[0]) / 2);
                iArrColumns[0] = (iTemporal + iArrColumns[0]);
            }
        }
        return (int) (iArrColumns[0]);
    }

}
