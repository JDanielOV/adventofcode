package adventofcode;

import java.util.Scanner;
import java.util.HashMap;
import java.util.Map;
import java.util.Vector;

public class Day6CustomCustoms {

    public void Day6CustomCustoms() {
        Vector<String> vList = new Vector();
        Scanner scan = new Scanner(System.in);
        String sPerson = " ";
        int iContQuestions = 0;
        while (sPerson.length() != 0) {
            while (sPerson.length() != 0) {
                sPerson = scan.nextLine();
                vList.add(sPerson);
            }
            iContQuestions += questionsConfirmed(vList);
            vList.clear();
            sPerson = scan.nextLine();
            if (sPerson.length() != 0) {
                vList.add(sPerson);
            }
        }
        System.out.println(iContQuestions);
    }

    public int questionsConfirmed(Vector<String> vList) {
        int iContQuestionsRepited = 0;
        Map<Character, Integer> mListGroup = new HashMap();
        for (int i = 0; i < vList.size() - 1; i++) {
            for (int j = 0; j < vList.get(i).length(); j++) {
                if (mListGroup.get(vList.get(i).charAt(j)) == null) {
                    mListGroup.put(vList.get(i).charAt(j), 1);
                } else {
                    mListGroup.put(vList.get(i).charAt(j), mListGroup.get(vList.get(i).charAt(j)) + 1);
                }
            }
        }
        for (Map.Entry<Character, Integer> entry : mListGroup.entrySet()) {
            Integer value = entry.getValue();
            System.out.println("value: " + value + " group: " + (vList.size() - 1));
            if (value == vList.size() - 1) {
                iContQuestionsRepited++;
            }
        }
        return iContQuestionsRepited;
    }
}
