package adventofcode;

import java.util.Vector;
import java.util.Scanner;

public class Day4PassportProcessing {

    public void Day4PassportProcessing() {
        Vector<String> vLits = new Vector();
        Scanner scan = new Scanner(System.in);
        String sText = " ";
        while (sText.length() != 0) {
            String sWord = "";
            while (sText.length() != 0) {

                sWord += sText;
                sText = scan.nextLine();
                sWord += " ";
            }
            vLits.add(sWord);
            sText = scan.nextLine();
        }
        System.out.println(vLits.size());
        int iContAcepted = 0;
        for (int i = 0; i < vLits.size(); i++) {
            System.out.println(vLits.get(i));
            iContAcepted += searchParameters(vLits.get(i));
        }
        System.out.println("cont: " + iContAcepted);
    }

    public int searchParameters(String sValue) {
//        System.out.println("");
        int iContParameters = 0;
        int iNumbers = 0;
        if (sValue.indexOf("byr") != -1) {
            iNumbers = Integer.parseInt(sValue.substring(sValue.indexOf("byr") + 4, sValue.indexOf("byr") + 8));
            System.out.print("-" + iNumbers + "-");
            if (iNumbers >= 1920 && iNumbers <= 2002) {
                iContParameters++;
            }

        }
        if (sValue.indexOf("iyr") != -1) {
            iNumbers = Integer.parseInt(sValue.substring(sValue.indexOf("iyr") + 4, sValue.indexOf("iyr") + 8));
            System.out.print("-" + iNumbers + "-");
            if (iNumbers >= 2010 && iNumbers <= 2020) {
                iContParameters++;
            }

        }
        if (sValue.indexOf("eyr") != -1) {
            iNumbers = Integer.parseInt(sValue.substring(sValue.indexOf("eyr") + 4, sValue.indexOf("eyr") + 8));
            System.out.print("-" + iNumbers + "-");
            if (iNumbers >= 2020 && iNumbers <= 2030) {
                iContParameters++;
            }
        }
        if (sValue.indexOf("hgt") != -1) {
//            System.out.print(sValue.substring(sValue.indexOf("cm") - 3, sValue.indexOf("cm")));
//            System.out.print(sValue.substring(sValue.indexOf("in") - 2, sValue.indexOf("in")));
            if (sValue.substring(sValue.indexOf("hgt")).indexOf("cm") != -1 && checkDigits(sValue.substring(sValue.indexOf("hgt"))) == true) {
                iNumbers = RcheckDigits(sValue.substring(sValue.indexOf("hgt")));
                if (iNumbers >= 150 && iNumbers <= 193) {
                    iContParameters++;
                }
            } else if (sValue.substring(sValue.indexOf("hgt")).indexOf("in") != -1 && checkDigits2(sValue.substring(sValue.indexOf("hgt"))) == true) {
                iNumbers = RcheckDigits2(sValue.substring(sValue.indexOf("hgt")));
                if (iNumbers >= 59 && iNumbers <= 76) {
                    iContParameters++;
                }
            }

        }
        if (sValue.indexOf("hcl") != -1) {
//            System.out.print(sValue.charAt(sValue.indexOf("hcl") + 4) + " ");
            if (sValue.substring(sValue.indexOf("hcl")).indexOf(" ") > 9 && sValue.indexOf("hcl") + 4 == sValue.indexOf("#")) {
//                System.out.print(sValue.substring(sValue.indexOf("#") + 1, sValue.indexOf("#") + 7));
                iContParameters += getHairColor(sValue.substring(sValue.indexOf("#") + 1, sValue.indexOf("#") + 7));
//                System.out.println("hcl: " + getHairColor(sValue.substring(sValue.indexOf("#") + 1, sValue.indexOf("#") + 7)));
            }
        }
        if (sValue.indexOf("ecl") != -1) {

            if (sValue.substring(sValue.indexOf("ecl")).indexOf(" ") > 6 && getEyeColor(sValue.substring(sValue.indexOf("ecl")))) {
                iContParameters++;
            }
        }
        if (sValue.indexOf("pid") != -1) {
//            System.out.println(sValue.substring(sValue.indexOf("pid") + 4, sValue.indexOf("pid") + 12));
            if (sValue.indexOf("pid") + 13 < sValue.length()) {
                iContParameters += getIdPassport(sValue.substring(sValue.indexOf("pid") + 4, sValue.indexOf("pid") + 13));
                if (getIdPassport(sValue.substring(sValue.indexOf("pid") + 4, sValue.indexOf("pid") + 13)) == 1) {
                    if (sValue.charAt(sValue.indexOf("pid") + 13) != ' ' && sValue.indexOf("pid") + 13 != sValue.length()) {
                        iContParameters--;
                    }
                }
            }

        }
        System.out.println("puntos: " + iContParameters);
        if (iContParameters == 7) {
            return 1;
        }
        return 0;
    }

    public int getHairColor(String sHair) {
        System.out.print("getHairColor:" + sHair + "-");
        for (int i = 0; i < sHair.length(); i++) {
            if (!((sHair.charAt(i) >= '0' && sHair.charAt(i) <= '9') || (sHair.charAt(i) >= 'a' && sHair.charAt(i) <= 'f'))) {
                return 0;
            }
        }
        return 1;
    }

    public int getIdPassport(String sId) {
        System.out.println("getIdPassport:" + sId + "-");
        for (int i = 0; i < sId.length(); i++) {
            if (!(sId.charAt(i) >= '0' && sId.charAt(i) <= '9')) {
                return 0;
            }
        }
        return 1;
    }

    public boolean checkDigits(String sWord) {
        String sTemp = sWord.substring(0, sWord.indexOf("cm"));
        String sCm = sTemp.substring(sTemp.length() - 3);
        System.out.println("checkDigits: " + sCm + "-");
        for (int i = 0; i < sCm.length(); i++) {
            if (!(sCm.charAt(i) >= '0' && sCm.charAt(i) <= '9')) {
                return false;
            }
        }
        if (sTemp.length() - sCm.length() != 4) {
            return false;
        }
        return true;
    }

    public boolean checkDigits2(String sWord) {
        String sTemp = sWord.substring(0, sWord.indexOf("in"));
        String sCm = sTemp.substring(sTemp.length() - 2);

        System.out.println("checkDigits2: " + sCm + "-");
        for (int i = 0; i < sCm.length(); i++) {
            if (!(sCm.charAt(i) >= '0' && sCm.charAt(i) <= '9')) {
                return false;
            }
        }
        if ((sTemp.length() - sCm.length()) != 4) {
            return false;
        }
        return true;
    }

    public int RcheckDigits2(String sWord) {
        String sTemp = sWord.substring(0, sWord.indexOf("in"));
        String sCm = sTemp.substring(sTemp.length() - 2);
        System.out.println("RcheckDigits2: " + sCm + "-");
        if ((sTemp.length() - sCm.length()) != 4) {
            return 0;
        }
        return Integer.parseInt(sCm);
    }

    public int RcheckDigits(String sWord) {
        String sTemp = sWord.substring(0, sWord.indexOf("cm"));
        String sCm = sTemp.substring(sTemp.length() - 3);
        System.out.println("RcheckDigits: " + sCm + "-");
        if ((sTemp.length() - sCm.length()) != 4) {
            return 0;
        }
        return Integer.parseInt(sCm);
    }

    public boolean getEyeColor(String sWord) {
        System.out.println("getEyeColor: " + sWord.substring(4, 7) + "-");
        if (sWord.substring(4, 7).equals("amb")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("blu")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("brn")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("gry")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("grn")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("hzl")) {
            return true;
        }
        if (sWord.substring(4, 7).equals("oth")) {
            return true;
        }
        return false;
    }
}
