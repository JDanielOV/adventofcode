package adventofcode;

import java.util.Scanner;
import java.util.Vector;

public class Day8HandheldHalting {
//706, muy baja
    public void Day8HandheldHalting() {
        Scanner scan = new Scanner(System.in);
        Vector<Integer> vInstructionsMade = new Vector();
        Vector<String> vInstructions = new Vector();
        String sInstructions = " ";
        int iContOperations = 0;
        while (!sInstructions.equals("00")) {
            sInstructions = scan.nextLine();
            vInstructions.add(sInstructions);
        }
        for (int i = 0; i < vInstructions.size() - 1; i++) {
            System.out.println("-" + vInstructions.get(i) + "-");
            if (vInstructions.get(i).startsWith("acc")) {
                iContOperations = returnContOperations(vInstructions.get(i), iContOperations);
                vInstructionsMade.add(i);
            } else if (vInstructions.get(i).startsWith("jmp")) {
//                System.out.println(i+"-"+);
                if (i == updateOperation(vInstructions.get(i), i, vInstructions.size()-1)) {
                    break;
                }
                i = updateOperation(vInstructions.get(i), i, vInstructions.size()-1);
                if (evaluateRepeat(vInstructionsMade, i)) {
                    break;
                }
                vInstructionsMade.add(i);
            } else if (vInstructions.get(i).startsWith("nop")) {
                continue;
            }
        }
        System.out.println(iContOperations);
    }

    public int returnContOperations(String sValue, int iCont) {
        if (sValue.indexOf("-") != -1) {
            return iCont - Integer.parseInt(sValue.substring(sValue.indexOf("-") + 1));
        }
        return iCont + Integer.parseInt(sValue.substring(sValue.indexOf("+") + 1));
    }

    public int updateOperation(String sValue, int iPosition, int iActivities) {
        if (sValue.indexOf("-") != -1) {
            if (Integer.parseInt(sValue.substring(sValue.indexOf("-") + 1)) > iPosition) {
                return iPosition;
            }
            return (iPosition - Integer.parseInt(sValue.substring(sValue.indexOf("-") + 1))) - 1;
        }
        if (Integer.parseInt(sValue.substring(sValue.indexOf("+") + 1)) > iActivities) {
            return iPosition;
        }
        return (iPosition + Integer.parseInt(sValue.substring(sValue.indexOf("+") + 1)));
    }

    public boolean evaluateRepeat(Vector<Integer> vList, int iActivity) {
        if (vList.indexOf(iActivity) != -1) {
            return true;
        }
        return false;
    }
}
