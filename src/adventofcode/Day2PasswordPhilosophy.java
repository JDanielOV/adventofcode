package adventofcode;

import java.util.Scanner;

public class Day2PasswordPhilosophy {

    public void Day2PasswordPhilosophy() {
        Scanner scan = new Scanner(System.in);
        String sData;
        int iContWords = 0;
        while (scan.hasNext()) {
            sData = scan.nextLine();
            if (sData.equals("00")) {
                break;
            }
            int iMin = Integer.parseInt(sData.substring(0, sData.indexOf('-')));
            int iMax = Integer.parseInt(sData.substring(sData.indexOf('-') + 1, sData.indexOf(' ')));
            int iCont = 0;
            char cLetter = sData.charAt(sData.indexOf(' ') + 1);
            String sTemp = sData.substring(sData.lastIndexOf(' '));
            for (int i = 0; i < sTemp.length(); i++) {
                if (cLetter == sTemp.charAt(i)) {
                    iCont++;
                }
            }
            System.out.println(iMin + " " + iMax + " " + iCont + " " + cLetter + "" + sTemp);
            if (iCont >= iMin && iCont <= iMax) {
                iContWords++;
            }
        }
        System.out.println(iContWords);
    }

    public void Day2PasswordPhilosophy_2() {
        Scanner scan = new Scanner(System.in);
        String sData;
        int iContWords = 0;
        while (scan.hasNext()) {
            sData = scan.nextLine();
            if (sData.equals("00")) {
                break;
            }
            int iBegin = Integer.parseInt(sData.substring(0, sData.indexOf('-'))) - 1;
            int iEnd = Integer.parseInt(sData.substring(sData.indexOf('-') + 1, sData.indexOf(' '))) - 1;
            if (sData.substring(sData.lastIndexOf(' ') + 1).charAt(iBegin) == sData.charAt(sData.indexOf(' ') + 1) && sData.substring(sData.lastIndexOf(' ') + 1).charAt(iEnd) != sData.charAt(sData.indexOf(' ') + 1)) {
                iContWords++;
            } else if (sData.substring(sData.lastIndexOf(' ') + 1).charAt(iEnd) == sData.charAt(sData.indexOf(' ') + 1) && sData.substring(sData.lastIndexOf(' ') + 1).charAt(iBegin) != sData.charAt(sData.indexOf(' ') + 1)) {
                iContWords++;
            }
        }
        System.out.println(iContWords);
    }

}
